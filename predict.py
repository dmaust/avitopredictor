﻿import numpy
import logging
import random as rnd
from sklearn.externals import joblib
import scipy.sparse as sp
from sklearn.utils import shuffle
from sklearn.preprocessing import StandardScaler
from sklearn.linear_model import SGDClassifier
from sklearn.naive_bayes import MultinomialNB
from sklearn.feature_selection import VarianceThreshold, SelectKBest, chi2

from sklearn.ensemble import GradientBoostingClassifier
from sklearn.ensemble import RandomForestClassifier, ExtraTreesClassifier
from matplotlib.pylab import plot

import build_vectors

cores = 3
cvSize = 1000000
save_frequency = 1
dataFolder = "/home/dmaust/avitopredictor"
scoreFolder = "/home/dmaust/avitopredictor/scores"

def sample_subspace(x,y):
    #indexes = rnd.sample(range(x.shape[0]), x.shape[0] / 2)  # Without replacement
    ratio = numpy.exp( - 2.6 - 1.0 * rnd.random() )
    feature_ratio = numpy.exp(- 1.0 * rnd.random() )
    indexes = numpy.random.random_integers(0, x.shape[0]-1, int(x.shape[0] * ratio) ) # With Replacement
    feature_indexes = numpy.random.random_integers(0, x.shape[1]-1, int(x.shape[1] * feature_ratio) ) # With Replacement
    return x[indexes][:, feature_indexes], y[indexes], feature_indexes

def sample(x,y, is_proved=None, repl=True, size=300000):
    if not repl:
        indexes = rnd.sample(range(x.shape[0]), size)  # Without replacement
    else:
        indexes = numpy.random.random_integers(0, x.shape[0]-1, size) # With Replacement

    if is_proved != None:
        indexes = numpy.hstack([indexes, numpy.nonzero(is_proved)[0]])
    return x[indexes], y[indexes]

def random_logistic():
    return 1. / (1. + numpy.exp((rnd.random() - 0.5) * 5))

def random_exp( center=1., scale=1. ):
    return center * numpy.exp((2.*rnd.random()-1. ) * scale)

def scale_pred(pred):
    """ Use the log odds ratio """
    pred = 0.99*pred + 0.5 * 0.01 # Shift toward 0 by a tiny amount
    return numpy.log(pred / (1 - pred)) # Take log odds ratio

def prec_score(pred, actual, q=0.95):

    perc_value = numpy.percentile(pred, q * 100.)
    total = sum(pred >= perc_value )
    correct = sum((actual == (pred > 0.)) & (pred >= perc_value ))
    if total == 0:
        return 0
    return float(correct) / float(total)

def prec_score_sorted(scores, q=0.95):
    total = int(len(scores) * (1- q))
    #print type(scores),total
    selected = scores[-total:]
    
    correct = len([score for score in selected if score[1]])
    if total == 0:
        return 0
    #print correct,'/',total
    return float(correct) / float(total)


def cv_score(pred, actual, verbose=False, min_q=0.95):
    values = sorted(zip(pred,actual), key=lambda x: x[0])
    qs = numpy.linspace(min_q, 1.0,40)[:-1]
    scores = [prec_score_sorted(values, q) for q in qs]
    if verbose:
        plot(qs,scores)
    return numpy.mean(scores)
 
def density(pred, actual, verbose=False, min_q=0.95):
    values = sorted(zip(pred,actual), key=lambda x: x[0])
    qs = [int(q) for q in numpy.linspace(min_q, 1.0,40)[:-1] * len(values)]
    lower_qs = qs[:-1]
    upper_qs = qs[1:]
    scores = [sum(samp[1] for samp in values[q[0]:q[1]]) / float(q[1]-q[0]) for q in zip(lower_qs, upper_qs)]
    if verbose:
        plot(lower_qs,scores)
    return scores
    
def save_results(predicted_scores, testItemIds):
    logging.info("Write results...")
    output_file = "solution.csv"
    logging.info("Writing submission to %s" % output_file)
    f = open(os.path.join(dataFolder,output_file), "w")
    f.write("id\n")

    for pred_score, item_id in sorted(zip(predicted_scores, testItemIds), reverse = True):
        f.write("%d\n" % (item_id))
    f.close()
 

import signal, os

cont = True

def handler(signum, frame):
    global cont
    cont = False    

def save_scores(predicted_scores, cv_scores, filename):
    joblib.dump((predicted_scores, cv_scores) , os.path.join(scoreFolder, filename))

def load_scores(filename):
    return joblib.load(os.path.join(scoreFolder, filename))

def build_scores(vt, fs, rf, testFeatures):
    predicted_scores = numpy.array([])
    batch = 100000
    for i in range(0, testFeatures.shape[0], batch):
        print i
        start = i
        end = min(i+batch, testFeatures.shape[0])
        selectedFeatures = testFeatures[start:end]
        vtFeatures = vt.transform(selectedFeatures)
        predicted_scores = numpy.hstack([predicted_scores, rf.predict_proba(fs.transform(vtFeatures).todense()).T[1]])
    return predicted_scores

def gbm_model():
    
    global cont

    logging.info("Loading GBM Data....")
    
    trainRfFeatures, testRfFeatures = build_vectors.loadTextFeatures()
    trainNumPcaFeatures, testNumPcaFeatures = build_vectors.loadNumPcaFeatures()

    trainRfFeatures = sp.hstack([trainRfFeatures, trainNumPcaFeatures], format='csr')
    testRfFeatures = sp.hstack([testRfFeatures, testNumPcaFeatures], format='csr')

    #Free Memory from old objects
    trainNumPcaFeatures = None
    testNumPcaFeatures = None

    trainTargets, testItemIds, close_hours, is_proved = build_vectors.loadLabels()

    vt = VarianceThreshold()
    vt.fit(trainRfFeatures[numpy.where(trainTargets)])

    trainRfFeatures = vt.transform(trainRfFeatures)
    testRfFeatures = vt.transform(testRfFeatures)

    trainRfFeatures, trainTargets, is_proved = shuffle(trainRfFeatures, trainTargets, is_proved, random_state=0)
    trainTargets = numpy.array(trainTargets)

    cvTargets = trainTargets[0:cvSize]
    cvRfFeatures = trainRfFeatures[0:cvSize]
    cvIsProved = is_proved[0:cvSize]

    trainRfFeatures = trainRfFeatures[cvSize:]
    trainTargets = trainTargets[cvSize:]
    is_proved = is_proved[cvSize:]

    predicted_scores = None
    cv_scores = None

    predicted_scores, cv_scores = load_scores('rf_scores_4.pkl')
    run = 5

    while cont:

        sample_X, sample_Y = sample(trainRfFeatures,trainTargets, repl=False) #, is_proved)

        vt = VarianceThreshold()
        sample_X = vt.fit_transform(sample_X)

        fs = ExtraTreesClassifier(n_jobs=cores, n_estimators=50, verbose=1)
        #fs = SelectKBest(n_jobs=cores, n_estimators=16)
        fs.fit(sample_X[:30000].todense(), sample_Y[:30000])

        sample_X = fs.transform(sample_X)
        logging.info("Sample_X.shape = %s", sample_X.shape)

        clf = GradientBoostingClassifier(n_estimators=100, verbose=True)
        clf.fit(sample_X.todense(), sample_Y)

        new_pred = scale_pred(build_scores(vt,fs,clf,testRfFeatures))
        new_cv_pred = scale_pred(build_scores(vt,fs,clf,cvRfFeatures))

        train_score = cv_score(clf.predict_proba(sample_X.todense()).T[1], sample_Y)

        weight = 1
        model_params = ''


        if predicted_scores is None:
            predicted_scores = new_pred * weight
            cv_scores = new_cv_pred * weight
        else:
            predicted_scores += new_pred * weight
            cv_scores += new_cv_pred * weight


        new_cv_score = cv_score(new_cv_pred, cvTargets)
        cum_cv_score = cv_score(cv_scores, cvTargets)


        if run % save_frequency == 0:
            save_scores(predicted_scores, cv_scores, "gbm_scores_%d.pkl"%run) 


        logging.info("Model:%d %s size:%d train: %s CV: %s Cum: %s", 
            run, model_params, sample_X.shape[0], train_score, new_cv_score, cum_cv_score)

        run += 1


def rf_model():
    
    global cont

    logging.info("Loading RF Data....")
    
    trainRfFeatures, testRfFeatures = build_vectors.loadTextFeatures()
    trainNumPcaFeatures, testNumPcaFeatures = build_vectors.loadNumericFeatures()

    trainRfFeatures = sp.hstack([trainRfFeatures, trainNumPcaFeatures], format='csr')
    testRfFeatures = sp.hstack([testRfFeatures, testNumPcaFeatures], format='csr')

    #Free Memory from old objects
    trainNumPcaFeatures = None
    testNumPcaFeatures = None

    trainTargets, testItemIds, close_hours, is_proved = build_vectors.loadLabels()

    vt = VarianceThreshold()
    vt.fit(trainRfFeatures[numpy.where(trainTargets)])
    trainRfFeatures = vt.transform(trainRfFeatures)
    testRfFeatures = vt.transform(testRfFeatures)


    trainRfFeatures, trainTargets = shuffle(trainRfFeatures, trainTargets, random_state=0)
    trainTargets = numpy.array(trainTargets)

    cvTargets = trainTargets[0:cvSize]
    cvRfFeatures = trainRfFeatures[0:cvSize]

    trainRfFeatures = trainRfFeatures[cvSize:]
    trainTargets = trainTargets[cvSize:]

    predicted_scores = None
    cv_scores = None

    predicted_scores, cv_scores = load_scores('rf_scores_0.pkl')
    run = 1

    while cont:

        sample_X, sample_Y = sample(trainRfFeatures,trainTargets, repl=False)

        vt = VarianceThreshold()
        sample_X = vt.fit_transform(sample_X)

        fs = ExtraTreesClassifier(n_jobs=cores, n_estimators=50, verbose=3)
        #fs = SelectKBest(n_jobs=cores, n_estimators=16)
        fs.fit(sample_X[:30000].todense(), sample_Y[:30000])

        sample_X = fs.transform(sample_X)
        logging.info("Sample_X.shape = %s", sample_X.shape)

        clf = ExtraTreesClassifier(n_jobs=cores, n_estimators=200, min_samples_split=10, verbose=3)
        clf.fit(sample_X.todense(), sample_Y)

        new_pred = scale_pred(build_scores(vt,fs,clf,testRfFeatures))
        new_cv_pred = scale_pred(build_scores(vt,fs,clf,cvRfFeatures))

        train_score = cv_score(clf.predict_proba(sample_X.todense()).T[1], sample_Y)

        weight = 1
        model_params = ''


        if predicted_scores is None:
            predicted_scores = new_pred * weight
            cv_scores = new_cv_pred * weight
        else:
            predicted_scores += new_pred * weight
            cv_scores += new_cv_pred * weight


        new_cv_score = cv_score(new_cv_pred, cvTargets)
        cum_cv_score = cv_score(cv_scores, cvTargets)


        if run % save_frequency == 0:
            save_scores(predicted_scores, cv_scores, "rf_scores_%d.pkl"%run) 


        logging.info("Model:%d %s size:%d train: %s CV: %s Cum: %s", 
            run, model_params, sample_X.shape[0], train_score, new_cv_score, cum_cv_score)

        run += 1


def lr_model():
    global cont

    logging.info("Loading LR Data....")

    trainFeatures, testFeatures = build_vectors.loadTextFeatures()
    trainNgFeatures, testNgFeatures = build_vectors.loadNgIdfFeatures()
    #trainFeatures, testFeatures = build_vectors.loadTfFeatures()
    trainPcaFeatures, testPcaFeatures = build_vectors.loadNumericFeatures()

    scaler = StandardScaler(with_mean=False)
    trainPcaFeatures = scaler.fit_transform(trainPcaFeatures)
    testPcaFeatures = scaler.transform(testPcaFeatures)
    #trainPcaFeatures = trainPcaFeatures[:,:100]
    #testPcaFeatures = testPcaFeatures[:,:100]

    trainTargets, testItemIds, close_hours, is_proved = build_vectors.loadLabels()

    testFeatures = sp.hstack([testFeatures, testNgFeatures, testPcaFeatures/3.], format='csr')
    testPcaFeatures = testNgFeatures = None

    trainFeatures = sp.hstack([trainFeatures, trainNgFeatures, trainPcaFeatures/3.], format='csr')
    trainPcaFeatures = trainNgFeatures = None

    sample_X = trainFeatures[numpy.where(trainTargets)]

    vt = VarianceThreshold()
    vt.fit(sample_X)
    trainFeatures = vt.transform(trainFeatures)
    testFeatures = vt.transform(testFeatures)
    

    #filter 
    threshold = 0.1
    #keep_indexes = numpy.where((1-trainTargets) | (close_hours > threshold) )
    #weight_indexes = numpy.where(is_proved)
    #trainFeatures = sp.vstack([trainFeatures[keep_indexes], trainFeatures[weight_indexes]], format='csr')
    #trainTargets = numpy.hstack([trainTargets[keep_indexes],trainTargets[weight_indexes]])

    

    logging.info("Shuffle Data....")
    trainFeatures, trainTargets = shuffle(trainFeatures, trainTargets, random_state=0)
    trainTargets = numpy.array(trainTargets)

    logging.info("Slice CV Set...")

    cvFeatures = trainFeatures[0:cvSize]
    cvTargets = trainTargets[0:cvSize]
    cvIsProved = is_proved[0:cvSize]
    #cvRfFeatures = trainRfFeatures[0:cvSize]
    #cvNumFeatures = trainNumFeatures[0:cvSize]

    trainFeatures = trainFeatures[cvSize:]
    #trainRfFeatures = trainRfFeatures[cvSize:]
    trainTargets = trainTargets[cvSize:]
    is_proved = is_proved[cvSize:]
    #trainNumFeatures = trainNumFeatures[cvSize:]

    logging.info("Feature preparation done, fitting model...")

    iterations = 1200

    run = 146 
    predicted_scores = None
    cv_scores = None
    predicted_scores, cv_scores = load_scores('lr_scores_145.pkl')

    while cont:

        alpha = numpy.exp(-14.5 - 1. * rnd.random() )
  
        if run % 1 == 0:
            loss = "modified_huber"
        else:
            loss = "log"

        clf = SGDClassifier(loss=loss, penalty="l2", random_state=123,
                        alpha=alpha, class_weight="auto", n_iter=iterations, shuffle=True)

        #clf = MultinomialNB()
        
        #multiplier = random_exp(center=0.10)
        #trainCombinedFeatures = sp.csr_matrix(sp.hstack([trainFeatures ,  multiplier * trainNumFeatures] ))
        #testCombinedFeatures = sp.hstack([testFeatures ,  multiplier * testNumFeatures] )
        #cvCombinedFeatures = sp.hstack([cvFeatures ,  multiplier * cvNumFeatures] )

        if run == 0:
            sample_X,sample_Y = trainFeatures,trainTargets
        else:
            sample_X, sample_Y = sample(trainFeatures,trainTargets, size=rnd.uniform(700000, 1000000))

        clf.fit(sample_X, sample_Y)

        new_pred = scale_pred(clf.predict_proba(testFeatures).T[1])
        new_cv_pred = scale_pred(clf.predict_proba(cvFeatures).T[1])

        weight = 1 #new_cv_score / float(1-new_cv_score)

        model_params = 'log_alpha: %s' %(numpy.log(alpha),)

        new_cv_score = cv_score(new_cv_pred, cvTargets)

        if predicted_scores is None:
            predicted_scores = new_pred * weight
            cv_scores = new_cv_pred * weight
        else:
            predicted_scores += new_pred * weight
            cv_scores += new_cv_pred * weight


        if run % save_frequency == 0:
            save_scores(predicted_scores, cv_scores, "lr_scores_%d.pkl" % run) 

        logging.info("Model:%d %s size:%d train: %s CV: %s Cum: %s", 
            run,
            model_params,
            sample_X.shape[0],
            cv_score(clf.predict_proba(sample_X).T[1], sample_Y), 
            new_cv_score,
            cv_score(cv_scores, cvTargets))
        run += 1




def main():
    logging.basicConfig(format = u'[LINE:%(lineno)d]# %(levelname)-8s [%(asctime)s]  %(message)s', level = logging.NOTSET, filename="predict.log")
    
    signal.signal(signal.SIGINT, handler)
    rf_model()

if __name__=="__main__":            
    main()            
    
    
    
